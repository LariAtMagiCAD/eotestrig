﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using EO.WebBrowser;
using EO.WebEngine;

namespace WebBrowser19140
{
   /// <summary>
   /// Simple one page WPF Window containing EO Web Browser module
   /// </summary>
   public partial class MainWindow : Window
   {
      /// <summary>
      /// Flag indicating is 19140 runtime initialized.
      /// </summary>
      private static bool IsRuntimeInitialized { get; set; } = false;

      /// <summary>
      /// Flag indicating should the Window be hide on Close event.
      /// </summary>
      private static bool HideOnClose { get; set; } = true;

      /// <summary>
      /// Flag indicating success of web page load.
      /// </summary>
      public bool? PageLoadedSuccessfully { get; private set; }

      /// <summary>
      /// Static EO Engine 19140 instance used for all Windows created from this class.
      /// </summary>
      private static Engine EOEngine { get; set; }

      /// <summary>
      /// Flag indicating was the EO loaded from GAC. Value is false if DLLs are local.
      /// </summary>
      public static bool EOLoadedFromGAC => typeof(Engine).Assembly.GlobalAssemblyCache;

      /// <summary>
      /// EO DLLs version number (19140)
      /// </summary>
      public static Version EOVersion => typeof(Engine).Assembly.GetName().Version;

      /// <summary>
      /// Temp directory for this module WebBrowser19140
      /// </summary>
      private static string ModuleTempDir { get; } = Path.GetFullPath(Path.Combine(System.IO.Path.GetTempPath(), Assembly.GetExecutingAssembly().GetName().Name));

      /// <summary>
      /// Custom EOWP.EXE path used only by this module
      /// </summary>
      private static string EowpExePath { get; } = Path.GetFullPath(Path.Combine(ModuleTempDir, $"eowp.{EOVersion.ToString(3)}.exe"));

      public MainWindow()
      {
         // Initialize EO runtime (only once)
         if (!IsRuntimeInitialized)
            RuntimeInitialization();

         // Initialize this Window component
         InitializeComponent();

         // Start listening to LoadCompleted event (in order to update PageLoadedSuccessfully)
         EOControl.WebView.LoadCompleted += WebViewOnLoadCompleted;

         // Start listening Window.Closing event in order to reuse Window
         Closing += OnClosing;

         // Start listening to Exit event in order to dispose everything
         if (Application.Current != null)
            Application.Current.Exit += OnExit;
         else
            AppDomain.CurrentDomain.ProcessExit += CurrentDomainOnProcessExit;
      }

      /// <summary>
      /// This initializes the EO Runtime specific settings etc.
      /// </summary>
      private static void RuntimeInitialization()
      {
         if (IsRuntimeInitialized)
            throw new ApplicationException("Runtime is already initialized!");

         // First start by setting runtime intialization flag true (small single thread test app no lock required)
         IsRuntimeInitialized = true;

         // Create module's temp directory if doesn't exist
         if (!Directory.Exists(ModuleTempDir))
            Directory.CreateDirectory(ModuleTempDir);

         // Create the custom EOWP.EXE for this module (WebBrowser 19140)
         EO.Base.Runtime.InitWorkerProcessExecutable(EowpExePath);
         //EO.Base.Runtime.EnableEOWP = true; // not in use...

         // Add license here... (if this would not be a public test rig app)
         //EO.WebBrowser.Runtime.AddLicense("");

         // Initialize EO Engine here
         EOEngine = Engine.Create(Guid.NewGuid().ToString());
      }

      /// <summary>
      /// Show the web browser dialog window
      /// </summary>
      /// <param name="url">Target URL for navigation.</param>
      /// <returns>Returns true if page load successful, false if page load failed and null if page load was interupted.</returns>
      public bool? ShowDialog(string url)
      {
         try
         {
            PageLoadedSuccessfully = null;
            EOControl.WebView.Url = url;
            ShowDialog();
         }
         catch (Exception e)
         {
            PageLoadedSuccessfully = false;
            MessageBox.Show(e.ToString(), Title, MessageBoxButton.OK, MessageBoxImage.Error);
            Close();
         }

         return PageLoadedSuccessfully;
      }

      /// <summary>
      /// Page LoadCompleted event handler for tracking the page load success.
      /// </summary>
      /// <param name="sender">Event sender.</param>
      /// <param name="loadCompletedEventArgs">Event arguments</param>
      private void WebViewOnLoadCompleted(object sender, LoadCompletedEventArgs loadCompletedEventArgs)
      {
         PageLoadedSuccessfully = true;
      }

      /// <summary>
      /// Window's OnClosing event handler
      /// </summary>
      /// <param name="sender">Event sender.</param>
      /// <param name="cancelEventArgs">Event arguments</param>
      private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
      {
         if (HideOnClose)
         {
            cancelEventArgs.Cancel = true;
            Hide();
         }
      }

      /// <summary>
      /// Domain exit handler.
      /// </summary>
      /// <param name="sender">Event sender.</param>
      /// <param name="eventArgs">Event arguments</param>
      private static void CurrentDomainOnProcessExit(object sender, EventArgs eventArgs)
      {
         HideOnClose = false;
         EO.Base.Runtime.Shutdown();
      }

      /// <summary>
      /// Host application exit handler.
      /// </summary>
      /// <param name="sender">Event sender.</param>
      /// <param name="exitEventArgs">Event arguments</param>
      private static void OnExit(object sender, ExitEventArgs exitEventArgs)
      {
         HideOnClose = false;
         EO.Base.Runtime.Shutdown();
      }
   }
}
