1. Fix the root path (REPLACE_PATH_HERE) in 'EO DLLs to GAC.txt' for all DLLs.
2. Load all EO DLLs to GAC using Visual Studio command promt (type VS to Windows search): 
   gacutil /il "D:\REPLACE_PATH_HERE\EOTestRig\EO DLLs to GAC.txt"
3. Launch solution, build and run.