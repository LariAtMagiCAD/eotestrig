﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace HostApp
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : Window
   {
      private const string BaseUrl = "https://www.google.com/search?q=essential+objects+{0}";
      private static int UrlCounter = 0;
      private static string Url => string.Format(BaseUrl, ++UrlCounter);

      #region WebBrowser Module Variables
      //private WebBrowser17243.MainWindow Browser17243 { get; } = new WebBrowser17243.MainWindow();
      //private static string EO17243LoadedFromGACStr => WebBrowser17243.MainWindow.EOLoadedFromGAC ? "GAC" : "local";
      //public static string EO17243VersionStr => WebBrowser17243.MainWindow.EOVersion.ToString(3);
      //public string EO17243ButtonText { get; } = $"Launch Web Browser with EO {EO17243VersionStr} ({EO17243LoadedFromGACStr})";

      private WebBrowser18131.MainWindow Browser18131 { get; } = new WebBrowser18131.MainWindow();
      private static string EO18131LoadedFromGACStr => WebBrowser18131.MainWindow.EOLoadedFromGAC ? "GAC" : "local";
      public static string EO18131VersionStr => WebBrowser18131.MainWindow.EOVersion.ToString(3);
      public string EO18131ButtonText { get; } = $"Launch Web Browser with EO {EO18131VersionStr} ({EO18131LoadedFromGACStr})";

      private WebBrowser18346.MainWindow Browser18346 { get; } = new WebBrowser18346.MainWindow();
      private static string EO18346LoadedFromGACStr => WebBrowser18346.MainWindow.EOLoadedFromGAC ? "GAC" : "local";
      public static string EO18346VersionStr => WebBrowser18346.MainWindow.EOVersion.ToString(3);
      public string EO18346ButtonText { get; } = $"Launch Web Browser with EO {EO18346VersionStr} ({EO18346LoadedFromGACStr})";

      private WebBrowser19083.MainWindow Browser19083 { get; } = new WebBrowser19083.MainWindow();
      private static string EO19083LoadedFromGACStr => WebBrowser19083.MainWindow.EOLoadedFromGAC ? "GAC" : "local";
      public static string EO19083VersionStr => WebBrowser19083.MainWindow.EOVersion.ToString(3);
      public string EO19083ButtonText { get; } = $"Launch Web Browser with EO {EO19083VersionStr} ({EO19083LoadedFromGACStr})";

      private WebBrowser19125.MainWindow Browser19125 { get; } = new WebBrowser19125.MainWindow();
      public static string EO19125LoadedFromGACStr => WebBrowser19125.MainWindow.EOLoadedFromGAC ? "GAC" : "local";
      public static string EO19125VersionStr => WebBrowser19125.MainWindow.EOVersion.ToString(3);
      public string EO19125ButtonText { get; } = $"Launch Web Browser with EO {EO19125VersionStr} ({EO19125LoadedFromGACStr})";

      private WebBrowser19140.MainWindow Browser19140 { get; } = new WebBrowser19140.MainWindow();
      public static string EO19140LoadedFromGACStr => WebBrowser19140.MainWindow.EOLoadedFromGAC ? "GAC" : "local";
      public static string EO19140VersionStr => WebBrowser19140.MainWindow.EOVersion.ToString(3);
      public string EO19140ButtonText { get; } = $"Launch Web Browser with EO {EO19140VersionStr} ({EO19140LoadedFromGACStr})";

      private WebBrowser19181.MainWindow Browser19181 { get; } = new WebBrowser19181.MainWindow();
      public static string EO19181LoadedFromGACStr => WebBrowser19181.MainWindow.EOLoadedFromGAC ? "GAC" : "local";
      public static string EO19181VersionStr => WebBrowser19181.MainWindow.EOVersion.ToString(3);
      public string EO19181ButtonText { get; } = $"Launch Web Browser with EO {EO19181VersionStr} ({EO19181LoadedFromGACStr})";
      #endregion WebBrowser Module Variables

      /// <summary>
      /// Initialize the HostApp MainWindow
      /// </summary>
      public MainWindow()
      {
         // Initialize Window components
         InitializeComponent();

         // Set button texts.
         //this.EO17243Button.Content = EO17243ButtonText;
         this.EO18131Button.Content = EO18131ButtonText;
         this.EO18346Button.Content = EO18346ButtonText;
         this.EO19083Button.Content = EO19083ButtonText;
         this.EO19125Button.Content = EO19125ButtonText;
         this.EO19140Button.Content = EO19140ButtonText;
         this.EO19181Button.Content = EO19181ButtonText;
      }

      /// <summary>
      /// Button click event handler
      /// </summary>
      /// <param name="sender">Button sending the event</param>
      /// <param name="args">Event arguments</param>
      private void Button_Click(object sender, RoutedEventArgs args)
      {
         // get sender / Button name
         string name = (sender as Button)?.Name;

         if (!string.IsNullOrEmpty(name))
         {
            bool? result = null;
            switch (name)
            {
               //case "EO17243Button":
               //   result = Browser17243.ShowDialog(Url);
               //   SetButtonColour(EO17243Button, result);
               //   break;
               case "EO18131Button":
                  result = Browser18131.ShowDialog(Url);
                  SetButtonColour(EO18131Button, result);
                  break;
               case "EO18346Button":
                  result = Browser18346.ShowDialog(Url);
                  SetButtonColour(EO18346Button, result);
                  break;
               case "EO19083Button":
                  result = Browser19083.ShowDialog(Url);
                  SetButtonColour(EO19083Button, result);
                  break;
               case "EO19125Button":
                  result = Browser19125.ShowDialog(Url);
                  SetButtonColour(EO19125Button, result);
                  break;
               case "EO19140Button":
                  result = Browser19140.ShowDialog(Url);
                  SetButtonColour(EO19140Button, result);
                  break;
               case "EO19181Button":
                  result = Browser19181.ShowDialog(Url);
                  SetButtonColour(EO19181Button, result);
                  break;
            }
         }
      }

      /// <summary>
      /// Change button color based on the flag (page load success indicator)
      /// </summary>
      /// <param name="button">Button to be modified</param>
      /// <param name="flag">page load success indicator flag</param>
      private void SetButtonColour(Button button, bool? flag)
      {
         if (flag.HasValue)
         {
            if (flag.Value)
               button.Background = new SolidColorBrush(Color.FromRgb(150, 255, 150));
            else
               button.Background = new SolidColorBrush(Color.FromRgb(255, 150, 150));
         }
         else
            button.Background = new SolidColorBrush(Color.FromRgb(255, 255, 150));
      }
   }
}
